﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;       
    public float restartDelay = 5f;            


    Animator anim;                          
    float restartTimer;
    public Text warningText;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            Debug.Log("Game Over");
            anim.SetTrigger("GameOver");

            
            restartTimer += Time.deltaTime;

            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public void ShowWarning(float enemyDistance)
    {
        warningText.text = string.Format("Warning! {0} m", Mathf.RoundToInt(enemyDistance));
        Debug.Log("warning");
        anim.SetTrigger("Warning");
    }

}