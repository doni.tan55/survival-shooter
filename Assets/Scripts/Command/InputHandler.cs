using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public PlayerShooting playerShooting;

    public PlayerHealth playerHealth;

    public float HealCooldown;
    public float HasteDuration;
    public float HasteCooldown;


    //Queue untuk menyimpan list command
    Queue<Command> commands = new Queue<Command>();

    void FixedUpdate()
    {
        //Menghandle input movement
        Command moveCommand = InputMovementHandling();
        if (moveCommand != null)
        {
            commands.Enqueue(moveCommand);

            moveCommand.Execute();
        }
    }

    void Update()
    {
        //Mengahndle shoot
        Command shootCommand = InputShootHandling();
        if (shootCommand != null)
        {
            shootCommand.Execute();
        }

        Command healCommand = Heal();
        if (HealCooldown < 0)
        {
            HealCooldown = 0;
            if (healCommand != null)
            {
                healCommand.Execute();
                HealCooldown = 10;
            }
        }
        else
        {
            HealCooldown -= Time.deltaTime;
        }
        

        Command hasteCommand = Haste();

        if (HasteCooldown < 0)
        {
            HasteCooldown = 0;
            if (hasteCommand != null)
            {

                hasteCommand.Execute();
                
                HasteCooldown = 20;
                HasteDuration = 7;
            }
            

        }
        else
        {
            HasteCooldown -= Time.deltaTime;
            if (HasteDuration < 0)
            {
                HasteDuration = 0;
                playerMovement.SetHaste(6); // kembalikan movementspeednya
            }
            else
            {
                HasteDuration -= Time.deltaTime;
            }

        }

        

    }

    Command InputMovementHandling()
    {
        //Check jika movement sesuai dengan key nya
        if (Input.GetKey(KeyCode.D))
        {
            return new MoveCommand(playerMovement, 1, 0);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            return new MoveCommand(playerMovement, -1, 0);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            return new MoveCommand(playerMovement, 0, 1);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            return new MoveCommand(playerMovement, 0, -1);
        }
        else if (Input.GetKey(KeyCode.Z))
        {
            //Undo movement
            return Undo();
        }
        else
        {
            return new MoveCommand(playerMovement, 0, 0); ;
        }
    }

    Command Undo()
    {
        //Jika Queue command tidak kosong, lakukan perintah undo
        if (commands.Count > 0)
        {
            Command undoCommand = commands.Dequeue();
            undoCommand.UnExecute();
        }
        return null;
    }

    Command InputShootHandling()
    {
        //Jika menembak trigger shoot command
        if (Input.GetButtonDown("Fire1"))
        {
            return new ShootCommand(playerShooting);
        }
        else
        {
            return null;
        }
    }


    Command Heal()
    {
        if (Input.GetKey(KeyCode.E))
        {
            
            
            //healing 
            return new HealCommand(playerHealth);
            
        }
        else
        {
            return null;
        }
    }

    Command Haste()
    {
        if (Input.GetKey(KeyCode.F))
        {
            
            //Haste
            return new HasteCommand(playerMovement);
        }
        else
        {
            return null;
        }
    }
}
