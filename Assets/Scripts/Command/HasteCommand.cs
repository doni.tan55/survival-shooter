using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HasteCommand : Command
{
    PlayerMovement movement;

    public HasteCommand(PlayerMovement Input)
    {
        movement = Input;
    }

    public override void Execute()
    {
        //healing
        movement.SetHaste(12);
    }

    public override void UnExecute()
    {

    }
}
