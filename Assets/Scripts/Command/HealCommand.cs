using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealCommand : Command
{

    PlayerHealth playerHealth;

    public HealCommand(PlayerHealth Input)
    {
        playerHealth = Input;
    }

    public override void Execute()
    {
        //healing
        playerHealth.IncreaseHealth(20);
    }

    public override void UnExecute()
    {

    }
}
